({
	 doInit :function(component, event) {
        debugger;
        var action = component.get("c.getcampignList");
        var recordId = component.get("v.recordId");
        action.setParams({recordId:recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result=response.getReturnValue();
                
                console.log("Success");
                component.set("v.ven",result);
            }
        });
        $A.enqueueAction(action)
    },
    
    doInitdata:function(component,event){
            debugger;
     var inputsel=  component.get("v.recordId");
         var action = component.get("c.getsubvenueList");
        action.setParams({ "accId" :inputsel });
    
      action.setCallback(this, function(response) {
      
            var state = response.getState();
            if (state === "SUCCESS") {
                
                component.set("v.vengrid", response.getReturnValue());
             }
          
          debugger;
         
        });
    $A.enqueueAction(action)
      
    },
     editRecord : function(component, event, helper) {
        debugger;
           
        var selectedItem = event.currentTarget;
        var inputsel = selectedItem.dataset.record;
       
    var editRecordEvent = $A.get("e.force:editRecord");
    editRecordEvent.setParams({
         "recordId": inputsel
   });
    editRecordEvent.fire();
       event.preventDefault();
},
    createRecord : function (component, event, helper) {
    var createRecordEvent = $A.get("e.force:createRecord");
    createRecordEvent.setParams({
        "entityApiName": "Venue__c"
    });
    createRecordEvent.fire();
         event.preventDefault();
},
    Savenext : function (component, event, helper) {
        debugger;
         var action = component.get("c.savevenue");
        var emps=component.get("v.venueIds");
       if (emps == '' || emps == null) {
           var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type" : "Error!",
                           "title": "Error!",
                          "message": "Please select sub venues",
                               });
                     toastEvent.fire();
           event.preventDefault();
      }else{
           
        action.setParams({ "emp" :emps });
       
          action.setCallback(this, function(response) {
            debugger;
            var state = response.getState();
            var name = response.getReturnValue();
            var state = response.getState();
              if (state === "SUCCESS"){
              var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type" : "Success!",
                           "title": "Success!",
                          "message": "Record saved Successfully",
                               });
                     toastEvent.fire();
                   
            }
            
        });
       // $A.enqueueAction(action);
         event.preventDefault();
        

     component.set('v.defaultTabId',"venuelayid");
      }
    },
    clickFunction : function(component,event,helper){
        debugger;
        var listReset= [];
        var ListOfjunc=component.get("v.vengrid");
     
       component.set("v.temp",listReset);
        var selecSub = component.get("v.temp");
        var checkedList = component.find("tokenAuraId");
        if(ListOfjunc.length === 1){
            if(component.find("tokenAuraId").get("v.value")=== true)
                selecSub.push(ListOfjunc[0].Id);
        }
        if(ListOfjunc.length === 2){
            if(component.find("tokenAuraId").get("v.value")=== true)
                selecSub.push(ListOfjunc[1]);
            
        }
        else{
        for (var i=0; i<ListOfjunc.length; i++) {
            if(i=== 0){
                continue;
            }
            else{
                if (checkedList[i-1].get("v.value") === true) {
                    selecSub.push(ListOfjunc[i].Id);
                } 
            }
          }
         
        }
        component.set("v.venueIds",selecSub);
    }
   
})