({
    searchVenues: function(component, event, helper) {
        component.set("v.isVenuePresent", true);
        var searchKey = event.getParam("searchKey");
        var action = component.get("c.findVenueByName");
        action.setParams({
            "searchKey": searchKey
        });
        action.setCallback(this, function(response) {
            component.set("v.venuelist", response.getReturnValue());
            helper.onSelect(component, event);
        });
        $A.enqueueAction(action);
    },
    getSelectedVenueOnChange: function(component, evt, helper) {
        var checkedList = component.find("checked");
        var result = checkedList.get("v.value");
        component.set("v.isMultipleVenueSelect", result);
        component.set("v.selectedsubvenue", null);
    },
    getSelectedVenueFromCheckbox: function(component, event, helper) {
        var venuelist = component.get("v.venueRecordLimitList");
        component.set("v.selectedsubvenue", venuelist);
        var isVenueSelected = event.currentTarget.checked;
        var selectedVenueId = event.currentTarget.dataset.record;
        var selectedsubvenues = component.get("v.juncobj");
        if (selectedVenueId != null || selectedVenueId != undefined) {
            if (isVenueSelected === true) {
                selectedsubvenues.push(selectedVenueId);
            }
        }
        if (selectedVenueId != null || selectedVenueId != undefined) {
            if (isVenueSelected === false) {
                selectedsubvenues.splice(selectedsubvenues.indexOf(selectedVenueId), 1);
            }
        }
        component.set("v.juncobj", selectedsubvenues);
    },
    getSelectedVenueFromRadio: function(component, event, helper) {
        $('table tr').click(function() {
            $(this).find('input:radio').prop('checked', true);
            var venueId = $(this).find('input:radio').attr('value');
            var selectedsubvenues = component.get("v.juncobj");
            if (venueId != null || venueId != undefined) {
                selectedsubvenues.push(venueId);
            }
            component.set("v.juncobj", selectedsubvenues);
        });
        component.set("v.juncobj", event.target.value);
        helper.handleChange(component, event);
        component.set('v.TabId', "venueid");
    },
    getSelectedSubVenueOnChange: function(component, event, helper) {
        helper.handleChange(component, event, helper);
    },
    getSubVenues: function(component, event, helper) {
     $('table tr').click(function() {
            $(this).find('input:radio').prop('checked', true);
            var venueId = $(this).find('input:radio').attr('value');
            var selectedsubvenues = component.get("v.juncobj");
            if (venueId != null || venueId != undefined) {
                selectedsubvenues.push(venueId);
            }
        });
        var selectedItem = event.currentTarget;
        var wholeRecord = selectedItem.dataset.record;
        var action = component.get("c.getsubVenueList");
        var checkboxtrue = component.get("v.isMultipleVenueSelect");
        action.setParams({ "subvenueId": wholeRecord });
        action.setCallback(this, function(response) {
            var state = response.getState();
            var subVenueList = response.getReturnValue();
            if (subVenueList == '') {
                helper.showMessage('Error!', 'Info!', 'There is no Subvenue available for selected Venue');
                event.preventDefault();
            } else if (state === "SUCCESS") {
                if (checkboxtrue == false) {
                    component.set("v.juncobj", []);
                }
                component.set("v.selectedsubvenue", []);
                component.set("v.selectedsubvenue", subVenueList);
            }
        });
        $A.enqueueAction(action);
    },
    redirectSelectedVenuesToNextTab: function(component, event, helper) {
        var checkboxesChecked = component.get("v.juncobj");
        var venuelist = component.get("v.venueRecordLimitList");
        if (checkboxesChecked == '' || checkboxesChecked == undefined) {
            helper.showMessage('Error!', 'Error!', 'Please select sub venues');
            event.preventDefault();
        } else {
            component.set('v.venueIds', "");
            component.set('v.venueIds', checkboxesChecked);
            component.set('v.TabId', "venuelayid");
            var cmpEvent = $A.get("e.c:componentCommunicationEvent");
            cmpEvent.setParams({
                "target": "A component event fired me. ",
                "targetIDs": "sample"
            });
            cmpEvent.fire();
        }
    },
    onClickfirstPage: function(component, event, helper) {
        component.set("v.currentPageNumber", 1);
        component.set("v.pageNumber", 1);
        if (component.get("v.pageNumber") === 1) {
            component.set("v.firstclick", true);
            component.set("v.prevclick", true);
            component.set("v.nextclick", false);
            component.set("v.lastclick", false);
            helper.paginationRecordLimitOnpageLoad(component);
        }
    },
    onClickprevPage: function(component, event, helper) {
        var pageN = component.get("v.pageNumber");
        pageN--;
        component.set("v.currentPageNumber", Math.max(component.get("v.currentPageNumber") - 1, 1));
        component.set("v.pageNumber", pageN);
        if (component.get("v.pageNumber") === 1) {
            component.set("v.firstclick", true);
            component.set("v.prevclick", true);
            component.set("v.nextclick", false);
            component.set("v.lastclick", false);
            helper.paginationRecordLimitOnpageLoad(component);
        } else if (component.get("v.pageNumber") < component.get("v.maxPageNumber")) {
            component.set("v.firstclick", false);
            component.set("v.prevclick", false);
            component.set("v.nextclick", false);
            component.set("v.lastclick", false);
            helper.paginationRecordLimitOnpageLoad(component);
        }
    },
    onClicknextPage: function(component, event, helper) {
        var pageN = component.get("v.pageNumber");
        pageN++;
        component.set("v.currentPageNumber", Math.min(component.get("v.currentPageNumber") + 1, component.get("v.maxPageNumber")));
        component.set("v.pageNumber", pageN);
        if (component.get("v.pageNumber") < component.get("v.maxPageNumber")) {
            component.set("v.firstclick", false);
            component.set("v.prevclick", false);
            component.set("v.nextclick", false);
            component.set("v.lastclick", false);
            helper.paginationRecordLimitOnpageLoad(component);
        } else if (component.get("v.pageNumber") == component.get("v.maxPageNumber")) {
            component.set("v.firstclick", false);
            component.set("v.prevclick", false);
            component.set("v.nextclick", true);
            component.set("v.lastclick", true);
            helper.paginationRecordLimitOnpageLoad(component);
        }
    },
    onClicklastPage: function(component, event, helper) {
        component.set("v.currentPageNumber", component.get("v.maxPageNumber"));
        component.set("v.pageNumber", component.get("v.maxPageNumber"));
        if (component.get("v.pageNumber") == component.get("v.maxPageNumber")) {
            component.set("v.firstclick", false);
            component.set("v.prevclick", false);
            component.set("v.nextclick", true);
            component.set("v.lastclick", true);
            helper.paginationRecordLimitOnpageLoad(component);
        }
    },
    refreshListOnTabSwitch: function(component, event, helper) {
        var target = event.getParam("target");
    },
    redirectToEventTab: function(component, event, helper) {
        component.set('v.TabId', "eventid");
    },
    getPreviousySelectedVenues: function(component,event,helper){
        var recordId = component.get("v.recordId");
        var action = component.get("c.getSelectedvenues");
        action.setParams({ "recordId" :recordId});
        action.setCallback(this, function(response) {
            var state = response.getState();
            var VenueList =  response.getReturnValue();
            component.set("v.venueListFordelete",VenueList);
            
        });
        $A.enqueueAction(action);
    },
    deleteVenue: function(component,event,helper){
        var selectedItem = event.currentTarget;
        var venRecordId = selectedItem.dataset.record;
        var recordId = component.get("v.recordId");
        var action = component.get("c.deletevenues");
        action.setParams({ "recordId" :recordId,
                          "venRecordId":venRecordId});
        action.setCallback(this, function(response) {
            var state = response.getReturnValue();
            if (state == "Error") {
                 helper.showMessage('Error!', 'Error!', 'Venue cannot be deleted of an Active Event');
                event.preventDefault();
            }else{
                var index = event.target.dataset.index;
                helper.removePreviouslySelectedVenues(component, index);
            }
        });
        $A.enqueueAction(action);
    }
})