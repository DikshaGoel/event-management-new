({
  	paginationRecordLimitOnpageLoad: function(component) {
        var records = component.get("v.venuelist"),
        pageNumber = component.get("v.pageNumber"),
        pageRecords = records.slice((pageNumber-1)*5, pageNumber*5);
        component.set("v.venueRecordLimitList", pageRecords);
  	},
  	onSelect:function (component){
      component.set("v.pageNumber",1);
      var resultEmp = component.get("v.venuelist");
      component.set("v.totalRecordsCount",resultEmp.length);
      component.set("v.oppRecordList",resultEmp);
      component.set("v.maxPage", Math.floor((resultEmp.length+9)/10));
      component.set("v.maxPageNumber", Math.floor((resultEmp.length+9)/10));
      if(resultEmp === null || resultEmp === undefined || resultEmp.length ===  0){
          component.set("v.pageNumber",0); 
          component.set("v.firstclick",true);
          component.set("v.prevclick",true);
          component.set("v.nextclick",true);
          component.set("v.lastclick",true);  
      }
      this.paginationRecordLimitOnpageLoad(component);    //calling helper's own method 
          component.set("v.firstclick",true);
          component.set("v.prevclick",true);
          component.set("v.nextclick",false);
          component.set("v.lastclick",false);       
      if(component.get("v.pageNumber") == component.get("v.maxPageNumber")){ 
          component.set("v.firstclick",true);
          component.set("v.prevclick",true);
          component.set("v.nextclick",true);
          component.set("v.lastclick",true); 
      }    
  	},
    handleChange : function(component,event,helper){
        var venuelist = component.get("v.selectedsubvenue");
        component.set("v.selectedsubvenue",venuelist);
        var isSubVenueSelected = event.currentTarget.checked;
        var selectedSubVenueId = event.currentTarget.dataset.record;
        var selectedsubvenues = component.get("v.juncobj");
        if(selectedSubVenueId != null || selectedSubVenueId != undefined){
            if(isSubVenueSelected === true){
                selectedsubvenues.push(selectedSubVenueId);
            } 
        }
        if(selectedSubVenueId != null || selectedSubVenueId != undefined){
            if(isSubVenueSelected === false){
                selectedsubvenues.splice(selectedsubvenues.indexOf(selectedSubVenueId),  1);
            } 
        }
        component.set("v.juncobj",selectedsubvenues);
    },
    removePreviouslySelectedVenues : function(component,index) {
        var venue = component.get("v.venueListFordelete");
        venue.splice(index, 1);
        component.set("v.venueListFordelete", venue);
    },
    showMessage: function(type, title, message) {
    var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "type" : type,
                           "title": title,
                          "message": message
                         });
    toastEvent.fire();
	}
})