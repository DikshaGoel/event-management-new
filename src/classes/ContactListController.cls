public class ContactListController {
	@AuraEnabled
    public static List<Contact> getContactList(String campaignId){
        Event_Venue__c eventVenue = [
            Select Id, Campaign__c From Event_Venue__c Where Id = :campaignId Limit 1
        ];
          System.debug('eventVenue'+eventVenue);
        List<Contact> contactList = new List<Contact>([SELECT Id, 
                                     		Name,Title
                                     		FROM Contact WHERE Id IN 
                                     		( SELECT ContactId 
                                             FROM CampaignMember 
                                             WHERE CampaignId =: eventVenue.Campaign__c
                                             AND ContactId != null)]);
        return contactList;
    }
}